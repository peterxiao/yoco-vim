" ======================================
" Vundle
" ======================================
set nocompatible  " iMproved
"filetype off      " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" My Bundles here:
"
" original repos on github
Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'godlygeek/tabular'
Bundle 'scrooloose/nerdcommenter'
Bundle 'scrooloose/syntastic'
Bundle 'airblade/vim-gitgutter'
Bundle 'Valloric/YouCompleteMe'
Bundle 'nathanaelkane/vim-indent-guides'
" vim-scripts repos
Bundle 'L9'
Bundle 'FuzzyFinder'
" non github repos
Bundle 'git://git.wincent.com/command-t.git'
" ...

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..

" ======================================
" brace
" ======================================

function! LeftBrace()
    if match(getline('.'), "\\C\\<struct\\>\\|\\<class\\>\\|\\<enum\\>\\|\\<union\\>") != -1 || match(getline(line('.')-1), "\\C\\<struct\\>\\|\\<class\\>\\|\\<enum\\>\\|\\<union\\>") != -1
        return "{\<CR> \<BS>\<CR>} ;\<ESC>\<UP>A"
    else
        return "{\<CR> \<BS>\<CR>}\<ESC>\<UP>A"
endfunction

" ======================================
" display
" ======================================
syntax on
set t_Co=256
colorscheme ir_black
set showmatch
set ruler
set relativenumber
" set cursorline
" set cursorcolumn
set laststatus=2
set splitright
set splitbelow
set nowrap
set hlsearch                            
set mps+=<:>
set guioptions-=T
set guioptions-=m
"set completeopt="longest,menu"
set completeopt="menu"
set gfn=Bitstream\ Vera\ Sans\ Mono\ 9
let g:load_doxygen_syntax=1
let g:doxygen_enhanced_color=0

" ======================================
" my key map
" ======================================
"nnoremap h <NOP>
"nnoremap j <NOP>
"nnoremap k <NOP>
"nnoremap l <NOP>

nnoremap ,s :w!<CR>
inoremap ,s <ESC>:w!<CR>
inoremap <C-S> <SPACE><BS><ESC>:w!<CR>a
inoremap <C-V> <SPACE><BS><ESC>pa
inoremap jj <ESC>
inoremap ;jj ;<ESC>
inoremap zz size_t 
"inoremap tn<SPACE> typename 
"inoremap tl<SPACE> template 
" easymotion
"nmap WW \w
"imap WW <ESC>\w
"nmap BB \b
"imap BB <ESC>\b

nmap ,c \cl
nmap ,u \cu 

autocmd filetype cpp,thorscript inoremap <expr> {{ LeftBrace()
map <silent> <C-PageUp> :bprev<CR>
map <silent> <C-PageDown> :bnext<CR>
map <silent> <C-H> :set invhlsearch<CR>
map <silent> <C-N> :cn<CR>
map <silent> <C-P> :cp<CR>
map <C-Q> <ESC>
noremap Q g<C-]>
noremap T <C-T>
map <C-J> viw"ay:echo "Word yanked"<CR>
map ,j viw"ay:echo "Word yanked"<CR>
map ,d yyP,cj
map <C-K> viw"ap
map <C-L> nviw"ap
map <M-Up> <C-W><Up>
map <M-Down> <C-W><Down>
map <M-Left> <C-W><Left>
map <M-Right> <C-W><Right>
map <C-Left> 4zh
map <C-Right> 4zl
map ,w <C-W>
map ,D :call InsertDoc()<CR>
map ,v <C-V>
map ,* :grep --exclude=tags --exclude-dir=.git --exclude-dir=doc '\<<C-R><C-W>\>' . -R<CR>
map ,8 :let @/='\<<C-R><C-W>\>'<CR>
map ,y :call StrToChars()<CR>

" ======================================
" my command
" ======================================
command! CO !cp % %.co; co %
command! COL !cp % %.co; co %; ssh yoco_xiao@ids95tw co -l teco/build/teco/%
command! Q q
command! Qa qa

" ======================================
" vim behavior settings
" ======================================
set nocp
set backspace=2
set ignorecase
set smarttab
set smartindent
set autoindent
set expandtab
set tabstop=4
set shiftwidth=4
set incsearch
"set foldmethod=marker
set wildmenu
set wildmode=list:longest
set wildignore=*.o,*.obj,*.bak,*.exe
set nrformats=hex
set undodir=~/.vim/undodir
set undofile
set undolevels=1000 "maximum number of changes tha can be undone
set undoreload=10000 "maximum number lines to save for undo on a buffer reload

set path+=/work/zp/platform/projects/protocol/include
set path+=/work/zp/platform/projects/common/include
set path+=/work/zp/platform/projects/framework/include
set path+=/work/zp/platform/projects/jarvis/include
set path+=/work/zp/platform/projects/language/include
set path+=/work/zp/platform/projects/network/include
set path+=/usr/include/c++/4.4
set path+=/home/yoco/package/tbb40_20110809oss/include
set path+=/usr/local/include

set makeprg=make

" ======================================
" indent_guides
" ======================================

let g:indent_guides_indent_levels = 10 " more than 10 is bad XD
"let g:indent_guides_guide_size = 1
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=233
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=234
autocmd BufReadPost *.cpp IndentGuidesEnable
autocmd BufReadPost *.cc IndentGuidesEnable
autocmd BufReadPost *.h IndentGuidesEnable
autocmd BufReadPost *.t IndentGuidesEnable
"let g:indent_guides_enable_on_vim_startup = 1

" ======================================
" tags
" ======================================
set tags+=/work/zp/platform/projects/tags
set tags+=~/tags/boost/boost_tags
set tags+=~/tags/websocketpp_tags
set tags+=~/tags/libc++_tags
set tags+=~/package/llvm-3.2.src/tags
set tags+=~/package/OpenCV-2.4.3/modules/tags
set tags+=./tags
map <f8> :!ctags -R .<cr>


" ======================================
" easy motion
" ======================================
nmap ? \\f
nmap { \\b
nmap } \\w

" ======================================
" FuzzyFinder
" ======================================
nmap ff :FufCoverageFile<CR>
" ======================================
" YouCompleteMe
" ======================================
let g:ycm_confirm_extra_conf = 0
let g:yc_add_preview_to_completeopt = 1
